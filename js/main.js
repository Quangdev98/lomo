

$(document).ready(function () {
	var win = $(window).width();
	if(win < 1025){
	}
	$(".toggle-search").click(function () {
		$(".wrap-search-header").toggleClass("active");
	});
	$(document).mouseup(function (e) {
		var container = $(".select-category");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-list-category-search').removeClass('active');
		}
	});

	$(".more-product").click(function () {
		$(".wrap-info-product").removeClass("box-min-height");
		$(this).css('display','none');
	});

	function setWidthText() {
		let showChar = 350;
		var ellipsestext = "...";
		var data = ['box-content-ykien p'];
		data.forEach(function (value) {

			$('.' + value).each(function () {
				var content = $(this).html();
				if (content.length > showChar) {
					var c = content.substr(0, showChar);
					var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp';
					$(this).html(html);
				}

			});
		});
	}
	setWidthText();

	var count = 1;
	$("#minus").click(function () {
		if (count > 1) {
			count--;
			$('#number-product').val(count);
			if($("#minus").hasClass('remove-minus')){
				$('#minus').removeClass('remove-minus')
			}
		} else {
			$('#minus').addClass('remove-minus')
		}
	});
	$("#plus").click(function () {
		count++;
		$('#number-product').val(count);
		$('#minus').removeClass('remove-minus')
	});



	// menu

});



// fix header
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}
$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});





// custome lomo 


$('#slide-main, #slide-feedback-main, #slide-product-detail').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
		},
	}
});
$('#pagination-slide').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 2,
		},
		375:{
			items: 3
		},
		540:{
			items: 4,
			margin: 15
		},
		1024:{
			margin: 25
		}
	}
});
$('#slide-product-same').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 2,
			margin: 15
		},
		541: {
			items: 3,
			margin: 15
		},
		1024: {
			items: 4,
			margin: 30
		}
	}
});
// 
let heiheader = $("header#header").height();
$("#slide-top").css('height', 'calc(100vh - ' + heiheader + 'px)')
console.log(heiheader);
// menu
$(".toggle-menu").click(function(){
	$(this).toggleClass("active")
	$("body").toggleClass("active-menu")
	$(".wrap-menu-main").toggleClass("active")
})
$(document).mouseup(function (e) {
	var container = $("header#header");
	if (!container.is(e.target) &&
		container.has(e.target).length === 0) {
			$(".toggle-menu").removeClass("active")
			$("body").removeClass("active-menu")
			$(".wrap-menu-main").removeClass("active")
	}
});
$("header#header .wrap-menu li i").click(function(){
	$(this).siblings("ul").slideToggle()
})
// count data 
// $('#box-count-data').waypoint(function () {
// 	$(".counter").each(function () {
// 		var $this = $(this),
// 			countTo = $this.attr("data-countto");
// 		countDuration = parseInt($this.attr("data-duration"));
// 		$({ counter: $this.text() }).animate(
// 			{
// 				counter: countTo
// 			},
// 			{
// 				duration: countDuration,
// 				easing: "linear",
// 				step: function () {
// 					$this.text(Math.floor(this.counter));
// 				},
// 				complete: function () {
// 					$this.text(this.counter);
// 				}
// 			}
// 		);
// 	});
// }, {
// 	offset: '100%'
// });

function resizeImage() {
	let arrClass = [
		{ class: 'resize-image-slide-video', number: (895 / 1590) }, 
		{ class: 'resize-item-pagination', number: (213 / 378) }, 
		{ class: 'resize-detail-product', number: (1043 / 784) }, 
		{ class: 'resize-paginetion-detail', number: (163 / 123) }, 
		{ class: 'resize-product', number: (500 / 376) }, 
		{ class: 'resize-category', number: (681 / 512) }, 
		{ class: 'box-image.img-big', number: (3 / 4) }, 
		{ class: 'box-image.img-small', number: (139 / 188) }, 
		{ class: 'resize-follow', number: (376 / 376) }, 
		{ class: 'resize-about', number: (257 / 188) }, 
		{ class: 'size-image-3', number: (357 / 782) }, 
		{ class: 'size-blog-2', number: (88 / 141) }, 
		{ class: 'size-blog-3', number: (88 / 141) }, 
	];
	for (let i = 0; i < arrClass.length; i++) {
		let width = $("." + arrClass[i]['class']).width();
		$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		// console.log(width);
		// console.log(arrClass);
		// console.log(width*arrClass[i]['number']);
	}
}
console.log($(".resize-product").width());
resizeImage();
$(window).on('resize', function () {
	resizeImage();
});

