var $swiperSelector = $('#product-hot-show');

$swiperSelector.each(function(index) {
    var $this = $(this);
    $this.addClass('swiper-slider-' + index);
    
    var dragSize = $this.data('drag-size') ? $this.data('drag-size') : 50;
    var freeMode = $this.data('free-mode') ? $this.data('free-mode') : false;
    var loop = $this.data('loop') ? $this.data('loop') : false;
    // var slidesDesktop = $this.data('slides-desktop') ? $this.data('slides-desktop') : 4;
    // var slidesTablet = $this.data('slides-tablet') ? $this.data('slides-tablet') : 3;
    // var slidesMobile = $this.data('slides-mobile') ? $this.data('slides-mobile') : 2.5;
    // var spaceBetween = $this.data('space-between') ? $this.data('space-between'): 20;

    var swiper = new Swiper('#product-hot-show', {
      direction: 'horizontal',
      loop: loop,
      freeMode: freeMode,
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 30
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 30
        },
        768: {
          spaceBetween: 20,
          slidesPerView: 2.5,
        },
        320: {
           slidesPerView: 1.5,
           spaceBetween: 15
        }
      },
      navigation: {
        nextEl: '.swiper-button-next1',
        prevEl: '.swiper-button-prev1'
      },
      scrollbar: {
        el: '.swiper-scrollbar1',
        draggable: true,
        dragSize: dragSize
      }
   });
});

var $swiperSelector2 = $('#slide-team-show');

$swiperSelector2.each(function(index) {
    var $this2 = $(this);
    $this2.addClass('swiper-slider-' + index);
    
    var dragSize = $this2.data('drag-size') ? $this2.data('drag-size') : 50;
    var freeMode = $this2.data('free-mode') ? $this2.data('free-mode') : false;
    var loop = $this2.data('loop') ? $this2.data('loop') : false;
    // var slidesDesktop = $this.data('slides-desktop') ? $this.data('slides-desktop') : 4;
    // var slidesTablet = $this.data('slides-tablet') ? $this.data('slides-tablet') : 3;
    // var slidesMobile = $this.data('slides-mobile') ? $this.data('slides-mobile') : 2.5;
    // var spaceBetween = $this.data('space-between') ? $this.data('space-between'): 20;

    var swiper2 = new Swiper('#slide-team-show', {
      direction: 'horizontal',
      loop: loop,
      freeMode: freeMode,
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 74
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 30
        },
        768: {
          spaceBetween: 20,
          slidesPerView: 2.5,
        },
        320: {
           slidesPerView: 1.5,
           spaceBetween: 15
        }
      },
      navigation: {
        nextEl: '.swiper-button-next2',
        prevEl: '.swiper-button-prev2'
      },
      scrollbar: {
        el: '.swiper-scrollbar2',
        draggable: true,
        dragSize: dragSize
      }
   });
});